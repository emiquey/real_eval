\documentclass[a4paper,10pt]{amsart}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{stmaryrd}
\usepackage{textcomp}
\usepackage{proof}
\usepackage{perfectcut}


%%%%%%%%%%% Macros %%%%%%%%%%%%%%%

\let\cut\perfectcut
\newcommand{\mumutilde}{$\lambda\mu\tilde\mu$}
\newcommand{\mut}{\tilde\mu}
\newcommand{\imp}{\to}
\newcommand{\limp}{\Rightarrow}
\newcommand{\red}{\rightsquigarrow}
\def\Bot{\bot\!\!\!\bot}
\newcommand{\tvv}[1]{|#1|_V}
\newcommand{\tv}[1]{|#1|}
\newcommand{\fv}[1]{\|#1\|}
\newcommand{\fvv}[1]{\|#1\|_V}
\newcommand{\eval}{\succ}
\newcommand{\Th}{\mathop{\textbf{th}}}
\newcommand{\real}{\Vdash}
\newcommand{\ureal}{\Vvdash}
\newcommand{\pole}{\Bot}


\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{corollary}{Corollary}
\theoremstyle{definition}
\newtheorem{definition}{Definition}




%opening
\title{Normalization by realizability of the \mumutilde-calculus}
\author{\'Etienne Miquey}
\date{May 2016}

\newcommand{\fig}[3]{\begin{figure}[h!]\framebox{\vbox{#3}}\caption{#1}\label{#2}\end{figure}}

\begin{document}
\maketitle
\section{\mumutilde}
\fig{Language}{fig:mumutilde}{
$$
\begin{array}{lc@{\hspace{2cm}}ccl}
   \text{Proofs  } & \Lambda 		& p & ::= &  V  \mid  \mu\alpha.c   \\
   \text{Values  } & \Lambda_V		& V & ::= &  a \mid \lambda a . p \\
   \text{Contexts} & \Pi 		& e & ::= &  E \mid \mut a.c   \\ 
   \text{Covalues} & \Pi_V		& E & ::= & \alpha \mid p\cdot e \\
   \text{Commands} & \Lambda \time \Pi 	& c & ::= &  \cut{p}{e} \\[1ex]
 \end{array}
 $$
}

\fig{Typing rules}{fig:typing}{
$$\begin{array}{c@{\qquad\qquad}c}
 \multicolumn{2}{c}{
  \infer[\textsc{cut}]{{\cut{p}{e}}:\Gamma\vdash \Delta}{
       \Gamma \vdash {p}:{A}  \mid \Delta & \Gamma \mid  {e}:{A} \vdash \Delta
 } 
 }
 \\
 &
 \\
 \infer[\text{\small \textsc{Ax}}^+]{\Gamma \vdash a:A \mid \Delta}{(a:A)\in\Gamma}
 &
 \infer[\text{\small \textsc{Ax}}^-]{\Gamma \mid \alpha:A \vdash \Delta}{(\alpha:A)\in\Delta}
 \\
 &
 \\
  \infer[\mu]{\Gamma \vdash {\mu\alpha.c} : {A}  \mid  \Delta }{{c}:(\Gamma\vdash \Delta, {\alpha}:{A})}
  &
  \infer[\mut]{\Gamma \mid  {\mut a.c} : {A} \vdash \Delta}    {{c}:(\Gamma,{a}:{A} \vdash \Delta)}
  \\
  &
  \\
   \infer[\imp_R]{\Gamma \vdash {\lambda a.p} : {A\imp B} \mid \Delta }{\Gamma,{a}:{A} \vdash {p}:{B} \mid  \Delta}
  &
  \infer[\imp_L]{\Gamma \mid  {q\cdot e} : {A\imp B}\vdash \Delta}{\Gamma\vdash {q}:{A} \mid \Delta & \Gamma \mid {e}:{B}\vdash \Delta}
\end{array}
  $$
}
 

\section{Call-by-value}
\subsection{Reduction rules}
$\mu$ gets the priority over $\mut$.
\fig{Call-by-value reduction}{fig:cbv}{
$$
\begin{array}{c@{\qquad \red \qquad}c}
  \cut{\mu \alpha.c}{e}	& c[e/\alpha] \\ 
  \cut{V}{\mut a.c}		& c[V/a] \\
  \cut{\lambda a.p}{q\cdot e} & \cut{q}{\mut a.\cut{p}{e}}
\end{array}    
$$
}

\subsection{Realizability model}
\begin{definition}
$\pole \subset \Lambda \times \Pi$ such that
$\forall c, c'\in \pole \land c\red c' \limp c\in\pole$
\end{definition}

\begin{definition}
 Semantics
 $$\begin{array}{rcl}
    \tvv{A\imp B} & = & \{\lambda x.t \mid \forall v\in\tvv{A}, t[x:=v]\in\tv{B}\}\\
    \fv{A} 	 & = & \{\pi \mid \forall t\in\tvv{A},\cut{t}{\pi}\in\pole\}\\
    \tv{A} 	 & = & \{t \mid \forall \pi\in\fv{A},\cut{t}{\pi}\in\pole\}\\
   \end{array}$$

\end{definition}
\begin{definition}We say that
\begin{enumerate}
 \item $\rho \Vdash \Gamma$ if for any $(a:A) \in\Gamma$, $\rho(a) \in \tvv{A}$
\item $\sigma \Vdash \Delta$ if for any $(\alpha:A) \in\Gamma$, $\rho(\alpha) \in \fv{A}$
 \end{enumerate}
\end{definition}

\begin{proposition}[Adequacy]
Let $\Gamma, \Delta$ be typing context, and $\rho \Vdash \Gamma$ and $\rho \Vdash \Delta$, then 
\begin{enumerate}
 \item if $\Gamma \vdash p:A\mid \Delta$, then $\rho(p) \in \tv{A}$
 \item if $\Gamma \mid e:A\vdash \Delta$, then $\rho(e) \in \fv{A}$
 \item if $c: \Gamma \vdash \Delta$, then $\rho(c) \in \pole$ 
\end{enumerate}
\end{proposition}
\begin{proof}
 By mutual induction, over the typing rules.
 \begin{itemize}
  \item Case \textsc{cut}: there is a type $A$, a proof $p$ and a context $e$ such that 
  $c = \cut{p}{e}$ and :
  $$ \infer[\textsc{cut}]{{\cut{p}{e}}:\Gamma\vdash \Delta}{
       \Gamma \vdash {p}:{A}  \mid \Delta & \Gamma \mid  {e}:{A} \vdash \Delta
 } $$
 
 By induction, we have $\rho(p)\in\tv{A}$ and $\rho(e)\in\fv{A}$, thus $\cut{\rho(p)}{\rho(e)}\in\pole$
 \item Case $\textsc{Ax}^+$:
 we have $$\infer[\text{\small \textsc{Ax}}^+]{\Gamma \vdash a:A \mid \Delta}{(a:A)\in\Gamma}$$,
 and $\rho\Vdash \Gamma$, thus $\rho(a)\in \tvv{A}\subset \tv{A}$.
 \item Case $\textsc{Ax}^-$ :
 we have $$\infer[\text{\small \textsc{Ax}}^-]{\Gamma \mid \alpha:A \vdash \Delta}{(\alpha:A)\in\Delta}$$
 and $\rho\Vdash \Delta$, thus $\rho(\alpha)\in \fv{A}$.

 \item Case $\mu$:
 there is a command $c$ such that 
  $$\infer[\mu]{\Gamma \vdash {\mu\alpha.c} : {A}  \mid  \Delta }{{c}:(\Gamma\vdash \Delta, {\alpha}:{A})}$$
 Let $e$ be a context in $\fv{A}$, by induction we have $\rho'=\rho[\alpha:=e]\Vdash \Delta,\alpha:A$,
 and thus $\rho'(c)=(\rho(c))[\alpha:=e] \in \pole$.
 By definition, 
 $$\cut{\rho(\mu \alpha.c)}{e}=\cut{\mu \alpha.\rho(c)}{e} \red {\rho(c)[\alpha := e]}\in\pole$$
 and we conclude by anti-reduction.
 
 \item Case $\mut$:
  there is a command $c$ such that 
  $$\infer[\mut]{\Gamma \mid  {\mut a.c} : {A} \vdash \Delta}    {{c}:(\Gamma,{a}:{A} \Vdash \Delta)}$$
  Let $V$ be a value in $\tvv{A}$, by induction we have $\rho'=\rho[a:=V]\vdash \Gamma,a:A$,
  and thus $\rho'(c)=(\rho(c))[a:=V] \in \pole$.
  By definition, 
  $$\cut{V}{\rho(\mut a.c)}=\cut{V}{\mut a.\rho(c)}\red {\rho(c)[a:=V]}\in\pole$$
  and we conclude by anti-reduction.
 \item Case $\imp_R$:
    $$\infer[\imp_R]{\Gamma \vdash {\lambda a.p} : {A\imp B} \mid \Delta }{\Gamma,{a}:{A} \vdash {p}:{B} \mid  \Delta}$$
    By definition, induction, if $v\in\tvv{A}$, then $\rho[a:=v]\Vdash \Gamma,a:A$ and $\rho(p)[a:=v]\in\tv{B}$.
    Thus $\lambda a.\rho(p) = \rho(\lambda a.p) \in \tvv{A\imp B}\subset \tv{A\imp B}$.
    
 \item  Case $\imp_L$:
   $$\infer[\imp_E]{\Gamma \mid  {q\cdot e} : {A\imp B}\vdash \Delta}{\Gamma\vdash {q}:{A} \mid \Delta & \Gamma \mid {e}:{B}\vdash \Delta}$$
   let $\lambda a.p\in\tvv{A\imp B}$, ie $p[a:=v]\in\tv{B}$ for any $v\in\tvv{A}$. By induction, $\rho(q)\in\tv{A}$, and 
   $$\cut{\lambda a.p}{q\cdot e} \red \cut{q}{\mut a.\cut{p}{e}}$$.
   It suffices to show by anti-reduction to show that $\mut a.\cut{p}{e}\in\fv{A}$.
   Once more, considering $v\in\tvv{A}$, as 
   $$\cut{v}{\mut a.\cut{p}{e}}\red \cut{p[a:=v]}{e}$$
   we conclude anti-reduction, using the induction hypothesis to show the $e\in\fv{B}$
   
 \end{itemize}

\end{proof}

\begin{corollary}
 For any context $\Gamma,\Delta$ and any command $c$, if $c:\Gamma\vdash \Delta$, then the reduction of $c$ terminates.
\end{corollary}

\section{Call-by-name}
\subsection{Reduction rules}
$\mut$ gets the priority over $\mu$.
\fig{Call-by-name reduction}{fig:cbv}{
$$
\begin{array}{c@{\qquad \red \qquad}c}
  \cut{p}{\mut a.c}		& c[p/a] \\
  \cut{\mu \alpha.c}{E}		& c[E/\alpha] \\ 
  \cut{\lambda a.p}{q\cdot e} 	& \cut{q}{\mut a.\cut{p}{e}}
\end{array}    
$$
}

\subsection{Realizability model}
\begin{definition}
$\pole \subset \Lambda \times \Pi$ such that
$\forall c, c'\in \pole \land c\red c' \limp c\in\pole$
\end{definition}

\begin{definition}
 Semantics
 $$\begin{array}{rcl}
    \fvv{A\imp B} & = & \{p\cdot e \mid \forall v\in\tvv{A}, t[x:=v]\in\tv{B}\}\\
%    \fv{A} 	 & = & \{\pi \mid \forall t\in\tvv{A},\cut{t}{\pi}\in\pole\}\\
    \tv{A} 	 & = & \{t \mid \forall \pi\in\fv{A},\cut{t}{\pi}\in\pole\}\\
   \end{array}$$

\end{definition}
\begin{definition}We say that
\begin{enumerate}
 \item $\rho \Vdash \Gamma$ if for any $(a:A) \in\Gamma$, $\rho(a) \in \tvv{A}$
\item $\sigma \Vdash \Delta$ if for any $(\alpha:A) \in\Gamma$, $\rho(\alpha) \in \fv{A}$
 \end{enumerate}
\end{definition}

\begin{proposition}[Adequacy]
Let $\Gamma, \Delta$ be typing context, and $\rho \Vdash \Gamma$ and $\rho \Vdash \Delta$, then 
\begin{enumerate}
 \item if $\Gamma \vdash p:A\mid \Delta$, then $\rho(p) \in \tv{A}$
 \item if $\Gamma \mid e:A\vdash \Delta$, then $\rho(e) \in \fv{A}$
 \item if $c: \Gamma \vdash \Delta$, then $\rho(c) \in \pole$ 
\end{enumerate}
\end{proposition}
\begin{proof}
 By mutual induction, over the typing rules.
 \begin{itemize}
  \item Case \textsc{cut}: there is a type $A$, a proof $p$ and a context $e$ such that 
  $c = \cut{p}{e}$ and :
  $$ \infer[\textsc{cut}]{{\cut{p}{e}}:\Gamma\vdash \Delta}{
       \Gamma \vdash {p}:{A}  \mid \Delta & \Gamma \mid  {e}:{A} \vdash \Delta
 } $$
 
 By induction, we have $\rho(p)\in\tv{A}$ and $\rho(e)\in\fv{A}$, thus $\cut{\rho(p)}{\rho(e)}\in\pole$
 \item Case $\textsc{Ax}^+$:
 we have $$\infer[\text{\small \textsc{Ax}}^+]{\Gamma \vdash a:A \mid \Delta}{(a:A)\in\Gamma}$$,
 and $\rho\Vdash \Gamma$, thus $\rho(a)\in \tvv{A}\subset \tv{A}$.
 \item Case $\textsc{Ax}^-$ :
 we have $$\infer[\text{\small \textsc{Ax}}^-]{\Gamma \mid \alpha:A \vdash \Delta}{(\alpha:A)\in\Delta}$$
 and $\rho\Vdash \Delta$, thus $\rho(\alpha)\in \fv{A}$.

 \item Case $\mu$:
 there is a command $c$ such that 
  $$\infer[\mu]{\Gamma \vdash {\mu\alpha.c} : {A}  \mid  \Delta }{{c}:(\Gamma\vdash \Delta, {\alpha}:{A})}$$
 Let $e$ be a context in $\fv{A}$, by induction we have $\rho'=\rho[\alpha:=e]\Vdash \Delta,\alpha:A$,
 and thus $\rho'(c)=(\rho(c))[\alpha:=e] \in \pole$.
 By definition, 
 $$\cut{\rho(\mu \alpha.c)}{e}=\cut{\mu \alpha.\rho(c)}{e} \red {\rho(c)[\alpha := e]}\in\pole$$
 and we conclude by anti-reduction.
 
 \item Case $\mut$:
  there is a command $c$ such that 
  $$\infer[\mut]{\Gamma \mid  {\mut a.c} : {A} \vdash \Delta}    {{c}:(\Gamma,{a}:{A} \Vdash \Delta)}$$
  Let $V$ be a value in $\tvv{A}$, by induction we have $\rho'=\rho[a:=V]\vdash \Gamma,a:A$,
  and thus $\rho'(c)=(\rho(c))[a:=V] \in \pole$.
  By definition, 
  $$\cut{V}{\rho(\mut a.c)}=\cut{V}{\mut a.\rho(c)}\red {\rho(c)[a:=V]}\in\pole$$
  and we conclude by anti-reduction.
 \item Case $\imp_R$:
    $$\infer[\imp_R]{\Gamma \vdash {\lambda a.p} : {A\imp B} \mid \Delta }{\Gamma,{a}:{A} \vdash {p}:{B} \mid  \Delta}$$
    By definition, induction, if $v\in\tvv{A}$, then $\rho[a:=v]\Vdash \Gamma,a:A$ and $\rho(p)[a:=v]\in\tv{B}$.
    Thus $\lambda a.\rho(p) = \rho(\lambda a.p) \in \tvv{A\imp B}\subset \tv{A\imp B}$.
    
 \item  Case $\imp_L$:
   $$\infer[\imp_E]{\Gamma \mid  {q\cdot e} : {A\imp B}\vdash \Delta}{\Gamma\vdash {q}:{A} \mid \Delta & \Gamma \mid {e}:{B}\vdash \Delta}$$
   let $\lambda a.p\in\tvv{A\imp B}$, ie $p[a:=v]\in\tv{B}$ for any $v\in\tvv{A}$. By induction, $\rho(q)\in\tv{A}$, and 
   $$\cut{\lambda a.p}{q\cdot e} \red \cut{q}{\mut a.\cut{p}{e}}$$.
   It suffices to show by anti-reduction to show that $\mut a.\cut{p}{e}\in\fv{A}$.
   Once more, considering $v\in\tvv{A}$, as 
   $$\cut{v}{\mut a.\cut{p}{e}}\red \cut{p[a:=v]}{e}$$
   we conclude anti-reduction, using the induction hypothesis to show the $e\in\fv{B}$
   
 \end{itemize}

\end{proof}

\begin{corollary}
 For any context $\Gamma,\Delta$ and any command $c$, if $c:\Gamma\vdash \Delta$, then the reduction of $c$ terminates.
\end{corollary}




\end{document}

