# Project Title

An adapation of Dagand, Rieg & Scherer code for the λµµ̃-calculus.  
The source code can be browsed at https://emiquey.gitlab.io/real_eval/.  
Their paper is available at: https://arxiv.org/pdf/1908.09123.pdf.  


### Prerequisites

Having Coq installed should enough.


### Installing

To compile the files, a simple call to the Makefile:
```
make
```
should do the job.
Then you can browse any of the file with CoqIDE or you favorite IDE.

## Authors

* **Étienne Miquey**


